﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sms.Data;
using Sms.Model;

namespace Sms.Test
{
    [TestClass]
    public class SmsDbContextTest
    {
        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            Database.SetInitializer<SmsDbContext>(new CreateDatabaseIfNotExists<SmsDbContext>());
        }

        [TestMethod]
        public void Using_orderdbContext_should_be_able_to_retrieve_orders_for_user()
        {
            using (var context = new SmsDbContext())
            {
                var user = GetUserToInsert();
                var product = GetProductToInsert();
                var order = CreateOrderForUser(user);
                List<Product> products = GetListOfProductsForOrder(product);
                AddProductsToOrder(order, products);

                context.Users.Add(user);
                context.Products.Add(product);
                context.Orders.Add(order);

                context.SaveChanges();

                var orderFromDb = context.Orders.First<Order>(o => o.Id == order.Id);
                Assert.IsTrue(orderFromDb.User.Id == user.Id);
            }
        }
        private User GetUserToInsert()
        {
            return new User
            {
                FirstName = "satyadeep",
                LastName = "behera",
                UserName = "satya.deep",
                Password = "password"
            };
        }

        private Product GetProductToInsert()
        {
            return new Product
            {
                Name = "Microsoft Visual Studio 2010 Ultimate",
                Amount = 20775m,
                Quantity = 1
            };
        }

        public Order CreateOrderForUser(User user)
        {
            return new Order
            {
                User = user,
                OrderNumber = "1",
                OrderDate = DateTime.Now,
            };
        }

        public void AddProductsToOrder(Order order, IEnumerable<Product> products)
        {
            foreach (var product in products)
            {
                if (order.Products == null) order.Products = new List<Product>();
                order.Products.Add(product);
                order.TotalAmount += product.Amount;
            }
        }

        private List<Product> GetListOfProductsForOrder(Product product)
        {
            List<Product> products = new List<Product>();
            products.Add(product);
            return products;
        }
    }
}
