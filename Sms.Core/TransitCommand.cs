﻿using Sms.Data.Repository;
using Sms.Model;

namespace Sms.Core
{
    public class TransitCommand:Command
    {
        private readonly IStoreRepository _storeRepository;

        public TransitCommand(IStoreRepository storeRepository)
        {
            _storeRepository = storeRepository;
        }

        public override void Execute(Order order)
        {
            _storeRepository.Insert(order);
            _storeRepository.SaveChanges();
        }

        public override void UnExecute(Order order)
        {
            throw new System.NotImplementedException();
        }
    }
}