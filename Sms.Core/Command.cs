﻿using Sms.Model;
namespace Sms.Core
{
    public abstract class Command
    {
        public abstract void Execute(Order order);
        public abstract void UnExecute(Order order);
    }
}