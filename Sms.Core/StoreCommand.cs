﻿using Sms.Data.Repository;
using Sms.Model;
namespace Sms.Core
{
    public class StoreCommand:Command
    {
        private readonly IStoreRepository _storeRepository;
        public StoreCommand(IStoreRepository storeRepository)
        {
            _storeRepository = storeRepository;
        }
        public override void Execute(Order order)
        {
            _storeRepository.Insert(order);
            _storeRepository.SaveChanges();

            foreach (var orderProduct in order.Products)
            {
                orderProduct.Quantity -= 1;
                //_storeRepository.Insert(orderProduct);
                _storeRepository.SaveChanges();
            }
        }
        public override void UnExecute(Order order)
        {
            throw new System.NotImplementedException();
        }
    }
}