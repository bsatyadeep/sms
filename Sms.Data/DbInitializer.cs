﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Sms.Model;

namespace Sms.Data
{
    public class DbInitializer : CreateDatabaseIfNotExists<SmsDbContext>
    {
        protected override void Seed(SmsDbContext context)
        {
            var sqlRepository = new SqlRepository(context);
            var p1 = new Product
            {
                Name = "Aniseed Syrup",
                Description = "12 - 550 ml bottles",
                Amount = 10.0000m,
                Quantity = 13,
                CreatedDate = DateTime.Now
            };
            var p2 = new Product
            {
                Name = "Northwoods Cranberry Sauce",
                Description = "12 - 12 oz jars",
                Amount = 40.0000m,
                Quantity = 6,
                CreatedDate = DateTime.Now
            };

            var s1 = new Store
            {
                Name = "A",
                Description = "Store A",
                Products = new List<Product> { p1, p2 }
            };

            var s2 = new Store
            {
                Name = "B",
                Description = "Store B",
                Products = new List<Product> { p1, p2 }
            };

            sqlRepository.Insert(s1);
            sqlRepository.Insert(s2);
        }
    }
}