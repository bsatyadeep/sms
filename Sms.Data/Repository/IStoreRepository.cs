﻿using System.Collections.Generic;
using Sms.Model;

namespace Sms.Data.Repository
{
    public interface IStoreRepository:IRepository
    {
        List<Product> GetProductStockInStore(int storeid,int productid);
        List<Product> GetProductsSetforCharity(int productid);
        Store GetStoreForProduct(int productid);
    }
}