﻿using System;
using System.Linq;

namespace Sms.Data.Repository
{
    public interface IRepository:IDisposable
    {
        void Delete<T>(T entity) where T : class;
        IQueryable<T> GetAll<T>() where T : class;
        void Insert<T>(T entity) where T : class;
        void SaveChanges();
    }
}