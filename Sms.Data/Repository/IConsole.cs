﻿namespace Sms.Data.Repository
{
    public interface IConsole
    {
        string ReadInput();
        void WriteOutputOnNewLine(string output);
        void WriteOutput(string output);
    }
}