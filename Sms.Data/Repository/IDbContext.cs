﻿using System;
using System.Data.Entity;

namespace Sms.Data.Repository
{
    public interface IDbContext:IDisposable
    {
        IDbSet<T> Set<T>() where T : class;
        int SaveChanges();
    }
}