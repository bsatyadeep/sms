﻿using System;

namespace Sms.Data.Repository
{
    public interface IUnitOfWork:IDisposable
    {
        void CommitTransaction();
        void StartTransaction();
    }
}