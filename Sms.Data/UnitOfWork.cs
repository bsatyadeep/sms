﻿using System.Transactions;
using Sms.Data.Repository;

namespace Sms.Data
{
    public class UnitOfWork:IUnitOfWork
    {
        private TransactionScope _transaction;
        public void CommitTransaction()
        {
            _transaction.Complete();
        }

        public void StartTransaction()
        {
            _transaction = new TransactionScope();
        }
        public void Dispose()
        {
            _transaction.Dispose();
        }
    }
}