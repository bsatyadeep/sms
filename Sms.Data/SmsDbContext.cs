﻿using System.Data.Entity;
using Sms.Data.Repository;
using Sms.Model;

namespace Sms.Data
{
    public class SmsDbContext:DbContext,IDbContext
    {
        public SmsDbContext()
        {
            //Database.SetInitializer(new DbInitializer(connectionName));
            Database.SetInitializer(new CreateDatabaseIfNotExists<SmsDbContext>());
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            SetPrimaryKeysOnEntities(modelBuilder);
            SetConcurrencyFieldsOnEntities(modelBuilder);
            SetValidationsOnUser(modelBuilder);
        }
        private void SetPrimaryKeysOnEntities(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasKey(u => u.Id);
            modelBuilder.Entity<Order>().HasKey(o => o.Id);
            modelBuilder.Entity<Product>().HasKey(p => p.Id);
            modelBuilder.Entity<Store>().HasKey(s => s.Id);
        }
        private static void SetConcurrencyFieldsOnEntities(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().Property(u => u.Version).IsConcurrencyToken();
            modelBuilder.Entity<Order>().Property(u => u.Version).IsConcurrencyToken();
            modelBuilder.Entity<Product>().Property(u => u.Version).IsConcurrencyToken();
            modelBuilder.Entity<Store>().Property(u => u.Version).IsConcurrencyToken();
        }
        private void SetValidationsOnUser(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().Property(u => u.FirstName).IsRequired().HasMaxLength(100);
            modelBuilder.Entity<User>().Property(u => u.LastName).IsRequired().HasMaxLength(100);
            modelBuilder.Entity<User>().Property(u => u.UserName).IsRequired().HasMaxLength(200);
            modelBuilder.Entity<User>().Property(u => u.Password).IsRequired();
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Store> Stores { get; set; }

        public new IDbSet<T> Set<T>() where T : class
        {
            return base.Set<T>();
        }
    }
}