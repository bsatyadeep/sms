﻿using Autofac;
using Sms.Data.Repository;

namespace Sms.Data
{
    public class DataModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new SmsDbContext()).As<IDbContext>().InstancePerRequest();
            builder.RegisterType<SqlRepository>().As<IRepository>().InstancePerRequest();
            builder.RegisterType<StoreRepository>().As<IStoreRepository>().InstancePerRequest();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            base.Load(builder);
        }
    }
}