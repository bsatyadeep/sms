﻿using System.Data.Entity;
using System.Linq;
using Sms.Data.Repository;

namespace Sms.Data
{
    public class SqlRepository:IRepository
    {
        private readonly IDbContext _context;
        public SqlRepository(IDbContext context)
        {
            _context = context;
        }
        public void Dispose()
        {
            _context?.Dispose();
        }
        public void Delete<T>(T entity) where T : class
        {
            GetEntities<T>().Remove(entity);
        }

        public IQueryable<T> GetAll<T>() where T : class
        {
            return GetEntities<T>().AsQueryable();
        }

        public void Insert<T>(T entity) where T : class
        {
            GetEntities<T>().Add(entity);
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        private IDbSet<T> GetEntities<T>() where T : class
        {
            return _context.Set<T>();
        }
    }
}