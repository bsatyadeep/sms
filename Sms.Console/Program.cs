﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sms.Core;
using Sms.Data;
using Sms.Model;
using Order = Sms.Model.Order;

namespace Sms.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new SmsDbContext())
            {
                var sqlRepository = new SqlRepository(context);
                var p1 = new Product
                {
                    Name = "Aniseed Syrup",
                    Description = "12 - 550 ml bottles",
                    Amount = 10.0000m,
                    Quantity = 1,
                    CreatedDate = DateTime.Now
                };
                var p2 = new Product
                {
                    Name = "Northwoods Cranberry Sauce",
                    Description = "12 - 12 oz jars",
                    Amount = 40.0000m,
                    Quantity = 1,
                    CreatedDate = DateTime.Now
                };
                var s1 = new Store
                {
                    Name = "A",
                    Description = "Store A",
                    Products = new List<Product> { p1, p2 }
                };
                var p3 = new Product
                {
                    Name = "Aniseed Syrup",
                    Description = "12 - 550 ml bottles",
                    Amount = 10.0000m,
                    Quantity = 10,
                    CreatedDate = DateTime.Now
                };
                var p4 = new Product
                {
                    Name = "Northwoods Cranberry Sauce",
                    Description = "12 - 12 oz jars",
                    Amount = 40.0000m,
                    Quantity = 10,
                    CreatedDate = DateTime.Now
                };
                var s2 = new Store
                {
                    Name = "B",
                    Description = "Store B",
                    Products = new List<Product> { p3, p4 }
                };
                var u1 = new User
                {
                    FirstName = "Satyadeep",
                    LastName = "Behera",
                    UserName = "satya.deep",
                    Password = "password"
                };

                sqlRepository.Insert(s1);
                sqlRepository.Insert(s2);
                sqlRepository.Insert(u1);
                sqlRepository.SaveChanges();

                //Place an Order
                Order order = new Order
                {
                    User = u1,
                    OrderNumber = "1",
                    OrderDate = DateTime.Now,
                };
                p1.Quantity = 1;
                List<Product> products = new List<Product> { p1 };
                AddProductsToOrder(order, products);

                //Execute Order
                StoreCommand storeCommand = new StoreCommand(new StoreRepository(context));
                storeCommand.Execute(order);

                //Get Product Availability
                //p1 = sqlRepository.GetAll<Product>().FirstOrDefault(p => p.Id == p1.Id);
                if (p1?.Quantity==0)
                {
                    Order transitfromOrder = new Order
                    {
                        User = u1,
                        OrderNumber = "2",
                        OrderDate = DateTime.Now,
                    };

                    //Find Store where the product is available
                    var productinstore = sqlRepository.GetAll<Product>()
                        .FirstOrDefault(p => p.Quantity > 0 && p.Name == p1.Name);
                    if (productinstore != null)
                    {
                        //Execute Transit Order From Store
                        List<Product> transitfromProducts = new List<Product> {productinstore};
                        AddProductsToOrder(transitfromOrder, transitfromProducts);
                        storeCommand.Execute(transitfromOrder);

                        //Execute Transit Order To Store
                        Order transittoOrder = new Order
                        {
                            User = u1,
                            OrderNumber = "3",
                            OrderDate = DateTime.Now,
                        };
                        p1.Quantity += 1;
                        List<Product> transittoProducts = new List<Product> { p1 };
                        AddProductsToOrder(transittoOrder, transittoProducts);
                        TransitCommand transitCommand = new TransitCommand(new StoreRepository(context));
                        transitCommand.Execute(transittoOrder);
                    }

                    //Product Moveed for Charity Charity
                    var p5 = new Product
                    {
                        Name = "Northwoods Cranberry Sauce",
                        Description = "12 - 12 oz jars",
                        Amount = 40.0000m,
                        Quantity = 10,
                        CreatedDate = DateTime.Now.AddYears(-5)
                    };
                    s2.Products.Add(p5);
                    sqlRepository.SaveChanges();


                   
                    DateTime checkDateTime = DateTime.Now.AddYears(-3);
                    var charityProducts = sqlRepository.GetAll<Product>().Where(p=> (p.CreatedDate < checkDateTime) && p.Quantity>0).ToList();
                    if (charityProducts.Count>0)
                    {
                        //Execute Charity Order
                        Order charityOrder = new Order
                        {
                            User = u1,
                            OrderNumber = "4",
                            OrderDate = DateTime.Now,
                        };
                        AddProductsToOrder(charityOrder, charityProducts);

                        CharityCommand charityCommand = new CharityCommand(new StoreRepository(context));
                        charityCommand.Execute(charityOrder);
                    }
                    
                }
            }
        }
        private static void AddProductsToOrder(Order order, IEnumerable<Product> products)
        {
            foreach (var product in products)
            {
                if (order.Products == null) order.Products = new List<Product>();
                order.Products.Add(product);
                order.TotalAmount += product.Amount * product.Quantity;
            }
        }
    }
}