﻿namespace Sms.Model
{
    public class ClosedOrderState:OrderState
    {
        public override bool IsClosed => true;
        public ClosedOrderState(Order currentOrder) : base(currentOrder)
        {
        }
        public override void Process()
        {
            throw new System.NotImplementedException();
        }
        public override void Cancel()
        {
            throw new System.NotImplementedException();
        }
        public override bool SendEmailsOnStatusChange()
        {
            throw new System.NotImplementedException();
        }
    }
}