﻿namespace Sms.Model
{
    public abstract class OrderState
    {
        internal readonly Order CurrentOrder;
        protected OrderState(Order currentOrder)
        {
            CurrentOrder = currentOrder;
        }
        public virtual bool IsNew => false;
        public virtual bool IsSubmitted => false;
        public virtual bool IsApproved => false;
        public virtual bool IsRejected => false;
        public virtual bool IsScheduled => false;
        public virtual bool IsShipped => false;
        public virtual bool IsClosed => false;
        public abstract void Process();
        public abstract void Cancel();
        public abstract bool SendEmailsOnStatusChange();
    }
}