﻿using System;
using System.Collections.Generic;
namespace Sms.Model
{
    public class User:BaseModel<int>
    {
        public DateTime? CreatedDate { get; set; }
        public DateTime? ChangedDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}