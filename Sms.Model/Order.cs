﻿using System;
using System.Collections.Generic;

namespace Sms.Model
{
    public class Order:BaseModel<int>
    {
        private OrderState _currentState;
        public Order()
        {
            _currentState = new NewOrderState(this);
        }

        public DateTime? CreatedDate { get; set; }
        public DateTime? ChangedDate { get; set; }
        public string OrderNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal TotalAmount { get; set; }
        public User User { get; set; }

        public ICollection<Product> Products { get; set; }

        public bool IsNew => _currentState.IsNew;
        public bool IsSubmitted => _currentState.IsSubmitted;
        public bool IsApproved => _currentState.IsApproved;
        public bool IsRejected => _currentState.IsRejected;
        public bool IsScheduled => _currentState.IsScheduled;
        public bool IsShipped => _currentState.IsShipped;
        public bool IsClosed => _currentState.IsClosed;
        public bool SendNotifications()
        {
            return _currentState.SendEmailsOnStatusChange();
        }
        public void ChangeState(OrderState orderState)
        {
            _currentState = orderState;
        }
        public void Process()
        {
            _currentState.Process();
        }
        public OrderState GetState()
        {
            return _currentState;
        }
    }
}