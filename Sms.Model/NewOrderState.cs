﻿namespace Sms.Model
{
    public class NewOrderState:OrderState
    {
        public override bool IsNew => true;

        public NewOrderState(Order currentOrder) : base(currentOrder)
        {
        }
        public override void Process()
        {
            var isSuccess = CurrentOrder.SendNotifications();
            if (isSuccess)
                CurrentOrder.ChangeState(new SubmittedOrderState(CurrentOrder));
        }
        public override void Cancel()
        {
            CurrentOrder.ChangeState(new ClosedOrderState(CurrentOrder));
        }
        public override bool SendEmailsOnStatusChange()
        {
            return true;
        }
    }
}