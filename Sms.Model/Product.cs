﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sms.Model
{
    public class Product:BaseModel<int>
    {
        [Required, StringLength(250)]
        public virtual string Name { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ChangedDate { get; set; }
        public decimal Amount { get; set; }
        public int Quantity { get; set; }

        public int StoreId { get; set; }
    }
}