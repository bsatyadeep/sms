﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sms.Model
{
    public class Store:BaseModel<int>
    {
        [Required, StringLength(250)]
        public virtual string Name { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}