﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sms.Model
{
    public abstract class BaseModel<T>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual T Id { get; set; }
        
        [StringLength(1000)]
        public virtual string Description { get; set; }

        [Timestamp]
        public Byte[] Version { get; set; }
    }
}