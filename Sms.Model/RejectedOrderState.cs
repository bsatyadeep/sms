﻿namespace Sms.Model
{
    public class RejectedOrderState:OrderState
    {
        public override bool IsRejected => true;
        public RejectedOrderState(Order currentOrder) : base(currentOrder)
        {
        }
        public override void Process()
        {
            throw new System.NotImplementedException();
        }
        public override void Cancel()
        {
            throw new System.NotImplementedException();
        }
        public override bool SendEmailsOnStatusChange()
        {
            throw new System.NotImplementedException();
        }
    }
}