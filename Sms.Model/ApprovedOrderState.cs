﻿namespace Sms.Model
{
    public class ApprovedOrderState:OrderState
    {
        public override bool IsApproved => true;
        public ApprovedOrderState(Order currentOrder) : base(currentOrder)
        {
        }
        public override void Process()
        {
            throw new System.NotImplementedException();
        }
        public override void Cancel()
        {
            throw new System.NotImplementedException();
        }
        public override bool SendEmailsOnStatusChange()
        {
            throw new System.NotImplementedException();
        }
    }
}