﻿namespace Sms.Model
{
    public class SubmittedOrderState:OrderState
    {
        public override bool IsShipped => true;
        public SubmittedOrderState(Order currentOrder) : base(currentOrder)
        {
        }
        public override void Process()
        {
            
        }
        public override void Cancel()
        {
        }
        public override bool SendEmailsOnStatusChange()
        {
            throw new System.NotImplementedException();
        }
    }
}