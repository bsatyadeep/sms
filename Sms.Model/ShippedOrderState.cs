﻿namespace Sms.Model
{
    public class ShippedOrderState:OrderState
    {
        public override bool IsShipped =>true;
        public ShippedOrderState(Order currentOrder) : base(currentOrder)
        {
        }
        public override void Process()
        {
            throw new System.NotImplementedException();
        }
        public override void Cancel()
        {
            throw new System.NotImplementedException();
        }
        public override bool SendEmailsOnStatusChange()
        {
            throw new System.NotImplementedException();
        }
    }
}